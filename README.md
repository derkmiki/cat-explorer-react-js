### Cat Browser

A simple browser app showing fundamentals of ReactJs.

### API URL

These are the details about the API used for the browser app.

https://api.thecatapi.com/v1/

### Endpoint to use

| **Purpose**                      | **Endpoint**                                                 | **Sample Response**                                          |
| :------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| Get List of breed                | https://api.thecatapi.com/v1/breeds                          | `[ 2    { 3        "weight": { 4            "imperial": "7  -  10", 5            "metric": "3 - 5" 6        }, 7        "id": "abys", 8        "name": "Abyssinian", 9        "cfa_url": "http://cfa.org/Breeds/BreedsAB/Abyssinian.aspx", 10        "vetstreet_url": "http://www.vetstreet.com/cats/abyssinian", 11        "vcahospitals_url": "https://vcahospitals.com/know-your-pet/cat-breeds/abyssinian", 12        "temperament": "Active, Energetic, Independent, Intelligent, Gentle", 13        "origin": "Egypt", 14        "country_codes": "EG", 15        "country_code": "EG", 16        "description": "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", 17        "life_span": "14 - 15", 18        "indoor": 0, 19        "lap": 1, 20        "alt_names": "", 21        "adaptability": 5, 22        "affection_level": 5, 23        "child_friendly": 3, 24        "dog_friendly": 4, 25        "energy_level": 5, 26        "grooming": 1, 27        "health_issues": 2, 28        "intelligence": 5, 29        "shedding_level": 2, 30        "social_needs": 5, 31        "stranger_friendly": 5, 32        "vocalisation": 1, 33        "experimental": 0, 34        "hairless": 0, 35        "natural": 1, 36        "rare": 0, 37        "rex": 0, 38        "suppressed_tail": 0, 39        "short_legs": 0, 40        "wikipedia_url": "https://en.wikipedia.org/wiki/Abyssinian_(cat)", 41        "hypoallergenic": 0, 42        "reference_image_id": "0XYvRd7oD", 43        "image": { 44            "id": "0XYvRd7oD", 45            "width": 1204, 46            "height": 1445, 47            "url": "https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg" 48        } 49    }, 50    ... 51  ] ` |
| Get List of Cat images per breed | https://api.thecatapi.com/v1/images/search?page=1&limit=10&breed_id=abys | `[ 2    { 3        "breeds": [ 4            { 5                "weight": { 6                    "imperial": "7  -  10", 7                    "metric": "3 - 5" 8                }, 9                "id": "abys", 10                "name": "Abyssinian", 11                "cfa_url": "http://cfa.org/Breeds/BreedsAB/Abyssinian.aspx", 12                "vetstreet_url": "http://www.vetstreet.com/cats/abyssinian", 13                "vcahospitals_url": "https://vcahospitals.com/know-your-pet/cat-breeds/abyssinian", 14                "temperament": "Active, Energetic, Independent, Intelligent, Gentle", 15                "origin": "Egypt", 16                "country_codes": "EG", 17                "country_code": "EG", 18                "description": "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.", 19                "life_span": "14 - 15", 20                "indoor": 0, 21                "lap": 1, 22                "alt_names": "", 23                "adaptability": 5, 24                "affection_level": 5, 25                "child_friendly": 3, 26                "dog_friendly": 4, 27                "energy_level": 5, 28                "grooming": 1, 29                "health_issues": 2, 30                "intelligence": 5, 31                "shedding_level": 2, 32                "social_needs": 5, 33                "stranger_friendly": 5, 34                "vocalisation": 1, 35                "experimental": 0, 36                "hairless": 0, 37                "natural": 1, 38                "rare": 0, 39                "rex": 0, 40                "suppressed_tail": 0, 41                "short_legs": 0, 42                "wikipedia_url": "https://en.wikipedia.org/wiki/Abyssinian_(cat)", 43                "hypoallergenic": 0, 44                "reference_image_id": "0XYvRd7oD" 45            } 46        ], 47        "id": "unPP08xOZ", 48        "url": "https://cdn2.thecatapi.com/images/unPP08xOZ.jpg", 49        "width": 2136, 50        "height": 2848 51    }, 52    ... 53  ] ` |
| Get Cat image and info           | https://api.thecatapi.com/v1/images/ozEvzdVM-                | `{ 2    "id": "ozEvzdVM-", 3    "url": "https://cdn2.thecatapi.com/images/ozEvzdVM-.jpg", 4    "breeds": [ 5        { 6            "weight": { 7                "imperial": "7 - 10", 8                "metric": "3 - 5" 9            }, 10            "id": "aege", 11            "name": "Aegean", 12            "vetstreet_url": "http://www.vetstreet.com/cats/aegean-cat", 13            "temperament": "Affectionate, Social, Intelligent, Playful, Active", 14            "origin": "Greece", 15            "country_codes": "GR", 16            "country_code": "GR", 17            "description": "Native to the Greek islands known as the Cyclades in the Aegean Sea, these are natural cats, meaning they developed without humans getting involved in their breeding. As a breed, Aegean Cats are rare, although they are numerous on their home islands. They are generally friendly toward people and can be excellent cats for families with children.", 18            "life_span": "9 - 12", 19            "indoor": 0, 20            "alt_names": "", 21            "adaptability": 5, 22            "affection_level": 4, 23            "child_friendly": 4, 24            "dog_friendly": 4, 25            "energy_level": 3, 26            "grooming": 3, 27            "health_issues": 1, 28            "intelligence": 3, 29            "shedding_level": 3, 30            "social_needs": 4, 31            "stranger_friendly": 4, 32            "vocalisation": 3, 33            "experimental": 0, 34            "hairless": 0, 35            "natural": 0, 36            "rare": 0, 37            "rex": 0, 38            "suppressed_tail": 0, 39            "short_legs": 0, 40            "wikipedia_url": "https://en.wikipedia.org/wiki/Aegean_cat", 41            "hypoallergenic": 0, 42            "reference_image_id": "ozEvzdVM-" 43        } 44    ], 45    "width": 1200, 46    "height": 800 47} ` |



### Folder Structure

- **public** - index html and files for public access
- **src**
1. **apis** - contains the API use for the project
2. **components** - all components to render a page
3. **pages** - pages called from routes
4. **utils -** hooks and utility function



### Quick Start

### `npm install`

This will download and install packages and dependencies required.

### `npm start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000/) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.  It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!