import React, { useState } from 'react'
import PackageContext from './utils/context'
import PropTypes from 'prop-types'

// Provider Component to pass state to consumer
const Provider = props => {
  const [state, setState] = useState({
    hasCatApiAlert: false,
    currentBreed: ''
  })
  return (
   <PackageContext.Provider
      value={{
        data: state,
        showCatApiAlert: () => setState({ ...state, hasCatApiAlert: true }),
        hideCatApiAlert: () => setState({ ...state, hasCatApiAlert: false }),
        setCurrentBreed: (breed) => setState({ ...state, currentBreed: breed })
      }}
    >
      {props.children}
    </PackageContext.Provider>
  )
}

Provider.propTypes = {
  children: PropTypes.any.isRequired
}

export default Provider
