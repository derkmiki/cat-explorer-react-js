import { useContext, useEffect } from 'react'
import PackageContext from '../utils/context'

// handles showing or hiding or ApiCatAlert
const useCatApiAlert = (apiResponse) => {
  const context = useContext(PackageContext)
  useEffect(() => {
    if (apiResponse.error === 'API_ERROR') {
      context.showCatApiAlert()
    } else {
      context.hideCatApiAlert()
    }
  }, [apiResponse])
}

export default useCatApiAlert
