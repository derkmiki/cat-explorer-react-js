import { useMemo } from 'react'
import { useLocation } from 'react-router-dom'

// custom hook for query parameter look up
const useQueryParams = () => {
  const { search } = useLocation()
  return useMemo(() => new URLSearchParams(search), [search])
}

export default useQueryParams
