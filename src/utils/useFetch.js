import { useState, useEffect } from 'react'

// custom hook to fetch from api
const useFetch = (url, requestOptions) => {
  const [data, setData] = useState({ error: '', data: null })

  useEffect(() => {
    fetch(url, requestOptions)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.status)
        } else {
          return res.json()
        }
      })
      .then((data) => setData({ error: '', data }))
      .catch((error) => {
        console.log(error)
        setData({ error: 'API_ERROR', data: null })
      })
  }, [url])

  return data
}

export default useFetch
