import React, { useContext } from 'react'
import BreedSelector from '../components/BreedSelector'
import CatCardDeck from '../components/CatCardDeck'
import { Row, Col } from 'react-bootstrap'
import PackageContext from '../utils/context'

const Home = () => {
  const context = useContext(PackageContext)
  return (
        <>
            <Row className="py-3">
                <Col sm={3}>
                    <p>Breed</p>
                    <BreedSelector />
                </Col>
            </Row>
            { context.data.currentBreed && <CatCardDeck /> }
        </>
  )
}

export default Home
