import React from 'react'
import { useParams } from 'react-router-dom'
import CatInfoCard from '../components/CatInfoCard'

const SingleCat = () => {
  const params = useParams()
  return (
        <>
        <CatInfoCard id={params.id} />
        </>
  )
}

export default SingleCat
