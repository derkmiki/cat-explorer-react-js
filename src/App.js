import React from 'react'
import Home from './pages/Home'
import SingleCat from './pages/SingleCat'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import CatApiAlert from './components/CatApiAlert'
import Provider from './Provider'

const App = () => (
  <Provider>
    <Container className="p-3">
      <CatApiAlert />
      <h1 className="header">Cat Browser</h1>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/single/:id" element={<SingleCat />} />
        </Routes>
      </BrowserRouter>
    </Container>
  </Provider>
)

export default App
