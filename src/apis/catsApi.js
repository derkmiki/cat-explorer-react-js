import useFetch from '../utils/useFetch'

const baseUrl = 'https://api.thecatapi.com/v1'

const requestOptions = {
  headers: { 'Content-Type': 'application/json' }
}

const getRequestOptions = {
  method: 'GET', ...requestOptions
}

// list of apis for cat
const catsApi = {
  listAllBreeds: () => useFetch(`${baseUrl}/breeds`, getRequestOptions),
  getImagesByBreedId: (page, limit, breedId) => useFetch(`${baseUrl}/images/search?page=${page}&limit=${limit}&breed_id=${breedId}`, getRequestOptions),
  getCatImageInfo: (id) => useFetch(`${baseUrl}/images/${id}`, getRequestOptions)
}

export default catsApi
