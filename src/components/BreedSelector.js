import React, { useContext, useEffect } from 'react'
import { Form } from 'react-bootstrap'
import catsApi from '../apis/catsApi'
import useQueryParams from '../utils/useQueryParams'
import PackageContext from '../utils/context'
import useCatApiAlert from '../utils/useCatApiAlert'

// Component to  select cat breed
const BreedSelector = function () {
  const context = useContext(PackageContext)

  const breeds = catsApi.listAllBreeds()
  useCatApiAlert(breeds)

  const params = useQueryParams()
  const breedId = params.get('breed')
  const currentBreed = context.data.currentBreed
  // set the breedId if provided from query parameter
  useEffect(() => {
    if (breedId && !currentBreed) {
      context.setCurrentBreed(breedId)
    }
  }, [])

  // change the breed selected from component, this will load the CatCardDeck
  const loadCatCardDeck = (e) => {
    context.setCurrentBreed(e.target.value)
  }

  return (
    <Form.Select aria-label="Select Breed" value={currentBreed} onChange={loadCatCardDeck}>
      <option>Select Breed</option>
      {breeds.data && breeds.data.map((breed) => <option key={breed.id} value={breed.id}>{breed.name}</option>)}
    </Form.Select>
  )
}

export default BreedSelector
