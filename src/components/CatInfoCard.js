import React from 'react'
import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
import catsApi from '../apis/catsApi'
import { useNavigate } from 'react-router-dom'
import useCatApiAlert from '../utils/useCatApiAlert'

// Single Cat information
const CatInfoCard = (props) => {
  const navigate = useNavigate()
  const catImageInfo = catsApi.getCatImageInfo(props.id)
  useCatApiAlert(catImageInfo)

  // function to goback to breedDeck where the cat belongs
  const goBackToBreed = () => {
    navigate('/?breed=' + catImageInfo.data.breeds[0].id)
  }
  return (
    catImageInfo.data && <Card>
    <Card.Header><Button onClick={goBackToBreed} variant="primary">Back</Button></Card.Header>
    <Card.Img variant="top" src={catImageInfo.data.url} />
    <Card.Body >
    <Card.Title>
        <h2>{catImageInfo.data.breeds[0].name}</h2>
        <h4>Origin: {catImageInfo.data.breeds[0].origin}</h4>
        <h5>Origin: {catImageInfo.data.breeds[0].temperament}</h5>
    </Card.Title>
    <Card.Text>
      {catImageInfo.data.breeds[0].description}
    </Card.Text>
    </Card.Body>
    </Card>
  )
}

CatInfoCard.propTypes = {
  id: PropTypes.string.isRequired
}

export default CatInfoCard
