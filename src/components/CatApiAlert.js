import React, { useContext } from 'react'
import { Alert } from 'react-bootstrap'
import PackageContext from '../utils/context'

// Component to  show cat api alert
const CatApiAlert = () => {
  const context = useContext(PackageContext)
  return (
    <Alert variant="danger" show={context.data.hasCatApiAlert}>
    Apologies but we could not load new cats for you at this time! Miau!
    </Alert>
  )
}

export default CatApiAlert
