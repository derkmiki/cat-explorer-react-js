import React from 'react'
import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { useNavigate } from 'react-router-dom'

const CatCard = (props) => {
  const navigate = useNavigate()
  const breedImage = props.breedImage
  // redirect to singleCat page wwhen cat is selected from component
  const loadSingleCat = (e) => {
    navigate('/single/' + breedImage.id)
  }
  return (
    <Card className="mb-3">
    <Card.Img variant="top" src={breedImage.url} />
    <Card.Body className="text-center">
        <Button onClick={loadSingleCat} variant="primary">View Details</Button>
    </Card.Body>
    </Card>
  )
}

CatCard.propTypes = {
  breedImage: PropTypes.shape({
    id: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }).isRequired
}

export default CatCard
