import React, { useState, useContext, useEffect, useRef } from 'react'
import CatCard from './CatCard'
import catsApi from '../apis/catsApi'
import { Row, Col, Button } from 'react-bootstrap'
import PackageContext from '../utils/context'
import useCatApiAlert from '../utils/useCatApiAlert'

// Component to  list cats under a breed
const CatCardDeck = () => {
  const context = useContext(PackageContext)
  const currentBreed = context.data.currentBreed
  const limit = 10
  const [page, setPage] = useState(1)
  const [loadedBreed, setLoadedBreed] = useState([])
  const previousLoadedBreedCount = useRef(0)
  const [previousBreed, setPreviousBreed] = useState('')

  // check if breed change reset the page and loaded breed states
  const checkBreedChange = () => {
    if (previousBreed !== currentBreed) {
      setPreviousBreed(currentBreed)
      setPage(1)
      setLoadedBreed([])
    }
  }
  checkBreedChange()

  const breedImages = catsApi.getImagesByBreedId(page, limit, currentBreed)
  useCatApiAlert(breedImages)

  // load more action increases page number
  const loadMore = () => {
    setPage((page) => page + 1)
  }

  // filter action to return unique breed deck
  const filterBreedLoaded = () => {
    previousLoadedBreedCount.current = loadedBreed.length
    const breedLoadedToFilter = [...loadedBreed, ...breedImages.data]
    return [...new Map(breedLoadedToFilter.map(item => [item.id, item])).values()]
  }

  // run whenever there is result from getImagesByBreedId API
  useEffect(() => {
    if (breedImages.data !== null) {
      setLoadedBreed(filterBreedLoaded())
    }
  }, [breedImages])

  return (
      <>
    <Row className="py-3">
    { loadedBreed.map((breedImage) => (
        <Col key={breedImage.id} sm={3} className="px-3">
            <CatCard breedImage={breedImage} />
        </Col>
    )
    )}
    </Row>
    <Row>
        <Col sm={2}>
        { (previousLoadedBreedCount.current !== loadedBreed.length) && <Button onClick={loadMore} variant="success">Load more</Button>}
        </Col>
    </Row>
    </>
  )
}

export default CatCardDeck
